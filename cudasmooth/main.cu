#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <sys/time.h>


using namespace std;
using namespace cv;

#define KERNEL_SIZE 5 // dimensao do filtro 5x5
#define KERNEL_VECTOR_SIZE 25
#define NUM_THREADS 1024
#define RODAR 10

void finishTime(struct timeval, struct timeval, float*);
__device__ unsigned char smooth(unsigned char* pixel, int i, int j, int w, int h);
__global__ void filtro(unsigned char *in, unsigned char *out, int w, int h);

int main(int argc, char* argv[]) {
	
	Mat in, out, RGB[3];
	int gray,i;
	unsigned char *dados_in, *dados_out;
	int tamanho, h, w, t;
	float tudo;
	
	struct timeval inicio;
	struct timeval fim;
	
	tudo = 0;
	
	//presença de arg define se a img vai ser aberta em GRAY ou COLOR
	if(argc > 2){
		gray = 1;
		in = imread(argv[1],CV_LOAD_IMAGE_GRAYSCALE);
		printf("gray img\n");
	}
	else{
		gray = 0;
		in = imread(argv[1],CV_LOAD_IMAGE_COLOR);
		printf("color img\n");
	}
	
	//obtém as dimensões da img de entrada
	h = in.rows;
	w = in.cols;
	
	// Aloca vetores para o device
	tamanho = w * h * sizeof(unsigned char);
	
	cudaMalloc((void**)&dados_in, tamanho);
	cudaMalloc((void**)&dados_out, tamanho);
	
	// Calcula numero de blocos necessarios em cada dimensão
	
	int blocks_x = (int)ceil((double)w/(double)NUM_THREADS);
	int blocks_y = h;

	// define numero de blocos e threads por blocos
	dim3 Blocos(blocks_x, blocks_y);
	dim3 tr_por_bloco(NUM_THREADS);
	
	t = 0;
	
	printf("\n Tempo em Segundos(s)\n");
	
	while(t < RODAR){

		gettimeofday(&inicio,0);
		
		//Img colorida
		if(!gray){
			
			//Divide img coloria in em 3 canais R , G , B
			split(in,RGB);
		
			// Passa o filtro no canal R
			cudaMemcpy(dados_in, (unsigned char*) RGB[0].data , tamanho,  cudaMemcpyHostToDevice);
			filtro<<<Blocos, tr_por_bloco>>>(dados_in, dados_out, w, h);
			cudaMemcpy((unsigned char*) RGB[0].data , dados_out, tamanho, cudaMemcpyDeviceToHost);
			
			// Passa o filtro no canal  G
			cudaMemcpy(dados_in, (unsigned char*) RGB[1].data, tamanho,  cudaMemcpyHostToDevice);
			filtro<<<Blocos, tr_por_bloco>>>(dados_in, dados_out, w, h);
			cudaMemcpy((unsigned char*) RGB[1].data, dados_out, tamanho, cudaMemcpyDeviceToHost);
			
			// Passa o filtro no canal  B
			cudaMemcpy(dados_in, (unsigned char*) RGB[2].data , tamanho,  cudaMemcpyHostToDevice);
			filtro<<<Blocos, tr_por_bloco>>>(dados_in, dados_out, w , h );
			cudaMemcpy((unsigned char*) RGB[2].data , dados_out, tamanho, cudaMemcpyDeviceToHost);\
	
		}
		//Grayscale
		else{
			
			//Passa o filtro no único canal da img em GRAY
			cudaMemcpy(dados_in, (unsigned char*) in.data , tamanho,  cudaMemcpyHostToDevice);
			filtro<<<Blocos, tr_por_bloco>>>(dados_in, dados_out, w, h);
			
			// Para nao aplicar o filtro 10 vezes na mesma img, os dados sao colocados na img somente na ULTIMA iteracao
			//if(t == RODAR - 1)
				cudaMemcpy((unsigned char*) in.data , dados_out, tamanho, cudaMemcpyDeviceToHost);			
		}
		
		//Printa o tempo de cada execucao, e calcula a média (no final)
		finishTime(inicio,fim,&tudo);
		
		t++;
	}
	
	// Merge dos canais e Free do vetor RGB[] (se nao for GRAYSCALE)
	if(!gray){
		
		merge(RGB,3,in);
		for(i = 0; i < 3; i++)
			RGB[i].release();
		
	}
	
	printf(" Media: %f\n",tudo/RODAR);
	
	//Salva img filtrada
	imwrite( "./smoothed.jpg", in );
	
	in.release();
	
	//Libera mem do CUDA
	cudaFree(dados_in);
	cudaFree(dados_out);
	
	return 0;
}

/* Funcao que calcula o tempo de cada execução, e a média final
* 
*/

void finishTime(struct timeval inicio, struct timeval fim, float *tudo){
		
	gettimeofday(&fim,0);
	float total = (fim.tv_sec + fim.tv_usec/1000000.0) - (inicio.tv_sec + inicio.tv_usec/1000000.0);
    //printf("tempo: %f\n", total);
    printf("\t%f", total);
    *tudo += total;
	printf("\n");
	
	
}

/*
 * DEVICE
 * Passa o filtro smooth em 1 pixel
 */
__device__ unsigned char smooth(unsigned char* pixel, int i, int j, int w, int h){
	
	int l, k;
	int sum;
	int raio = KERNEL_SIZE/2;
	
	sum = 0;
	for(l = i - raio; l <= i + raio; l++) {
		
		for(k = j - raio; k <= j + raio; k++) {
			
			if(l >= 0 && k >= 0 && l < h && k < w) {
				sum += pixel[l*w + k];
			}
			else
				sum += 0;
		}
	}
	return sum/KERNEL_VECTOR_SIZE;
}
/*
 * Funcao KERNEL
 * Funçao que determina o indice do pixel a ser tratado
 */
__global__ void filtro(unsigned char *in, unsigned char *out, int w, int h) {
	
	int i, j;
	
	
	//Calcula indice do pixel que vai ser tratado

	i = blockIdx.y;
	j = blockIdx.x*blockDim.x + threadIdx.x;

	// chama a funcao que calcula o novo valor do pixel
	out[i*w+j] = smooth(in, i, j, w, h);
}
